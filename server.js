const express = require('express');
const bodyparser = require('body-parser');
const PythonShell = require('python-shell');
const schedule = require('node-schedule');

const app = express();
const mongoose = require('./connection');
var router = require('./batdongsandata.route');
//router(app);
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());
app.use('/', router);
const port = process.env.PORT||3001;
const db = mongoose.connection;

schedule.scheduleJob({hour: 8, minute: 0}, () => updatedatabase());
function updatedatabase() {
    try{
        console.log('update database');
        var options = {
            pythonOptions: ['-u'],
            scriptPath: './',
        };

        PythonShell.PythonShell.run('temp.py', options, function (err, results) {
            if (err) throw err;
            // results is an array consisting of messages collected during execution
            console.log('results: %j', results);
        });

    } catch (e) {
        console.log(e);
    }
}

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.listen(port, () => {
    console.log('Started on port '+ port);
});
module.exports = app;
