const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const BdsController = require('./batdongsandata.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/page/:id', BdsController.getBDSByIdPage);
router.get('/', BdsController.getAllBDS);
router.get('/detail/:_id', BdsController.getBDSByMaBDS);
router.post('/search', BdsController.searchBDS);
router.get('/quanhuyen', BdsController.getQuanHuyen);
router.get('/loaibds', BdsController.getLoaiBds);
module.exports = router;
