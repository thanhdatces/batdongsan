from bs4 import BeautifulSoup
import urllib.request as urllib
from pymongo import MongoClient
client = MongoClient("mongodb://thagdet:detlun232@ds045137.mlab.com:45137/batdongsan")
db = client.batdongsan
bds = db.batdongsandatas
matched = 0
modified = 0
output = []
url = 'https://nhadat24h.net/ban-bat-dong-san-da-nang-nha-dat-da-nang-s295704/'
for i in range(1,15):
    page = urllib.urlopen(url + str(i)).read()
    soup = BeautifulSoup(page, 'html.parser')
    results = []
    table = soup.find('div', attrs={'id': 'divlistitemketquatimkiem'})
    lists = table.find_all('div', attrs={'class': 'dv-txt'})
    for list in lists:
        results.append(list.find('a').get('href'))
    for result in results:
        pagedetail = urllib.urlopen('https://nhadat24h.net' + result)
        soup1 = BeautifulSoup(pagedetail, 'html.parser')
        title = soup1.find('div', attrs={'class': 'dv-ct-detail'}).find('h1').getText().replace('  ','').replace('"','')
        Content = soup1.find('div', attrs={'class': 'dv-tb-tsbds'}).find('table')
        rows = Content.find_all('tr')
        details = []
        for row in rows:
            detail = row.find_all('td')
            details.append(detail[1].getText().replace('  ','').replace('\n','').replace('"',''))

        if('Tỷ' in details[0]):
            details[0] = int(float(details[0].replace('Tỷ','').replace(',','.')) * 1000)
        elif('Triệu' in details[0]):
            details[0] = int(float(details[0].replace('Triệu','')))
        else:
            details[0] = 0
        details[1] = int(float(details[1].replace('M²','')))
        details[2] = int(details[2])

        images = soup1.find('div', attrs={'class': 'myPanelClass'}).find_all('a')
        imageLink = []
        for image in images:
            imageLink.append('https://nhadat24h.net' + image.get('href'))
        details.append(imageLink)
        profile = soup1.find('div', attrs={'class': 'dv-cont-dt'})
        details.append(profile.find('label').getText())
        profile_infors = profile.find('p').find_all('label')
        details.append(profile_infors[0].getText().replace('  ','').replace('\n','').replace('\r','').replace('"',''))
        details.append(profile_infors[1].getText().replace('  ','').replace('\n','').replace('\r','').replace('"',''))
        details.append(profile_infors[2].getText().replace('  ','').replace('\n','').replace('\r','').replace('"','').split('-'))

        description =''
        descriptions = soup1.find('div', attrs={'id': 'ContentPlaceHolder2_divContent'}).find_all('p')
        for des in descriptions:
            des = str(des).split('<br/>')
            for de in des:
                description = description + BeautifulSoup(de, 'html.parser').getText() + '\n'
        details.append(description)
        map = []
        if soup1.find('input', attrs={'id': 'txtLAT'}):
            map.append(soup1.find('input', attrs={'id': 'txtLAT'}).get('value'))
            map.append(soup1.find('input', attrs={'id': 'txtLON'}).get('value'))
        details.append(map)
        change = bds.update_one({'_id':details[2]},{'$set': {'title':title, 'tongtien': details[0], 'dientich': details[1], '_id': details[2], 'tinhthanh': details[3], 'loaitin': details[4], 'ngaydang': details[5],'quanhuyen': details[6],'vitri': details[7],'loaibds': details[8],'huong': details[9],'hinhanh':details[10],'tennguoiban': details[11], 'congtyban': details[12], 'diadiemcongty': details[13], 'sodienthoai': details[14], 'mota': details[15], 'map': details[16]}}, upsert=True)
        matched = matched + change.matched_count
        modified = modified + change.modified_count
print("matched file: " + str(matched))
print("modified file: " + str(modified))
