const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let bdsSchema = new Schema({
    id: {   type: Number },
    title: {type: String, require: false },
    tongtien: {type: Number, require: false },
    dientich: {type: Number, require: false },
    _id: {type: Number, require: true },
    tinhthanh: {type: String, require: false },
    loaitin: {type: String, require: false },
    ngaydang: {type: String, require: false },
    quanhuyen: {type: String, require: false },
    vitri: {type: String, require: false },
    loaibds: {type: String, require: false },
    huong: {type: String, require: false },
    hinhanh: [{type: String, require: false }],
    tennguoiban: {type: String, require: false },
    congtyban: {type: String, require: false },
    diadiemcongty: {type: String, require: false },
    sodienthoai: [{type: String, require: false }],
    mota: {type: String, require: false },
    map: [{type: String, require: false }],
});


// Export the model
module.exports = mongoose.model('batdongsandata', bdsSchema);
