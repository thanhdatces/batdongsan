const batdongsan = require('./batdongsandata.model');
const AHP = require ('ahp');

//Get Question By ID
exports.getBDSByIdPage = async (req, res) => {
    let limit = 10;
    if(req.query.limit) limit = req.query.limit;
    let data = await batdongsan.find().skip((req.params.id-1)*10)
        .sort({id:1}).limit(parseInt(limit));
    console.log(data.length);
    res.json({
        sucess: "true",
        data: data
    });
};

exports.getBDSByMaBDS = async (req, res) => {
    let data = await batdongsan.find({_id: req.params._id});
    res.json({
        sucess: "true",
        data: data
    });
};

async function getByID(id){
    let data = await batdongsan.find({_id: id});
    return data;
}
//mongoimport -h ds045137.mlab.com:45137 -u thagdet -p detlun232 -d batdongsan -c batdongsandatas --file bds.json --jsonArray --mode=upsert


//Get All Question
exports.getAllBDS = function (req, res) {
    batdongsan.find()
        .sort({id:1})
        .limit(10)
        .exec(function(error, posts) {
            res.json({
                sucess: "true",
                data: posts
            });
        });
};

exports.getLoaiBds = function (req, res) {
    res.json({
        sucess: "true",
        data: [
            "",
            "Đất biệt thự",
            "Căn hộ chung cư",
            "Đất dự án",
            "ShopHouse",
            "Condotel",
            "Đất dịch vụ",
            "Đất thổ cư",
            "Đất trang trại",
            "Mặt bằng, Nhà Xưởng",
            "Nhà cấp 4",
            "Nhà mặt phố",
            "Nhà trong ngõ dưới 3m",
            "Nhà trong ngõ trên 3m",
            "Phòng trọ, nhà trọ",
            "Cửa hàng",
            "Văn phòng",
            "Khách sạn"
        ]
    });
};

exports.getQuanHuyen = function (req, res) {
    res.json({
        sucess: "true",
        data: [
            "",
            "Huyện Đảo Hoàng Sa",
            "Huyện Hòa Vang",
            "Quận Cẩm Lệ",
            "Quận Dương Kinh",
            "Quận Hải Châu",
            "Quận Liên Chiểu",
            "Quận Ngũ Hành Sơn",
            "Quận Sơn Trà",
            "Quận Thanh Khê"
        ]
    });
};
//search
exports.searchBDS = function (req, res) {
    Example_data = {
        "tongtienmin": 1000,
        "tongtienmax":1500,
        "loaibds": "Đất thổ cư",
        "dientichmin":100,
        "dientichmax": 200,
        "quanhuyen":"Quận Ngũ Hành Sơn"};
    Exemple_priority = [1,2,3,4];

    var ahpContext = new AHP();

    let output = ahpContext.run();
    batdongsan.find({
        tongtien: {$gt: req.body.data.tongtienmin, $lt: req.body.data.tongtienmax},
        loaibds: {'$regex' : req.body.data.loaibds},
        dientich: {$gt: req.body.data.dientichmin, $lt: req.body.data.dientichmax},
        quanhuyen: {'$regex' : req.body.data.quanhuyen},
        })
        .sort({id:1})
        .limit(30)
        .exec(async function(error, posts) {
            for (let i = 0; i < posts.length; i++) {
                ahpContext.addItem(posts[i]._id);
            }

            ahpContext.addCriteria(['price', 'area', 'type']);

            let rankPrice = [];
            let rankArea = [];
            let rankType = [];

            for (let i = 0; i < posts.length - 1; i++) {
                for (let j = i + 1; j < posts.length; j++) {
                    rankPrice.push([posts[i]._id, posts[j]._id, (posts[i].tongtien < posts[j].tongtien) ? posts[j].tongtien * 10 / posts[i].tongtien : (posts[i].tongtien > posts[j].tongtien) ? posts[j].tongtien / (posts[i].tongtien * 10) : 1]);
                    rankArea.push([posts[i]._id, posts[j]._id, (posts[i].dientich > posts[j].dientich) ? posts[i].dientich * 10 / posts[j].dientich : (posts[i].dientich < posts[j].dientich) ? posts[j].dientich / (posts[i].dientich * 10) : 1]);
                }
                rankType.push(1);
            }
            rankType.push(1);
            //{sort: number}
            ahpContext.rankCriteriaItem('price', rankPrice);
            ahpContext.rankCriteriaItem('area', rankArea);
            ahpContext.setCriteriaItemRankByGivenScores('type', rankType);

            ahpContext.rankCriteria([
                ['price', 'area', req.body.priority[0] / req.body.priority[2]],
                ['price', 'type', req.body.priority[0] / req.body.priority[1]],
                ['area', 'type', req.body.priority[2] / req.body.priority[1]]
            ]);
            output = ahpContext.run();
            var keys = [];
            for (let key in output.rankedScoreMap){
                keys.push(key);
            }
            var unsortedMap = new Map();
            for(let i=0; i<output.rankedScores.length; i++) {
                unsortedMap.set(output.rankedScores[i], keys[i]);
            }
            var sortedMap = [];

            output.rankedScores.sort(function(a, b){
                return b - a;
            }).map(function(key) {
                sortedMap.push(unsortedMap.get(key));
            });
            console.log(sortedMap);
            var datas = [];
            for (let id of sortedMap){
                let data = await batdongsan.find({_id: id});
                datas.push(data);
            }
            console.log('search: ' + datas.length);
            res.json({
                sucess: "true",
                data: datas
            });
        });
};
